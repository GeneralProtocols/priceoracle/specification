# Introduction

This specification describes a set of rules, expectations and processes that allows service providers to set up and deliver verifiable pricing data inside Bitcoin Cash smart contracts.

## Motivation

In order to fully realize the potential of smart contracts on the Bitcoin Cash blockchain, we need access to reliable and consistent information that can be verified and used to determine contract outcomes.
With access to such data, we believe smart contracts can enable or improve processes in a diverse set of markets ranging from supply chains to insurance.

## Definitions

- `Attestation period`: A measure for how frequently an oracle creates price messages.
- `Access point`: A location where oracle messages can be acquired.
- `Contract manager`: An entity that handles smart contract details such as construction, tracking and redemption.
- `Message distribution`: Distribution of signed messages through various channels.
- `Message production`: Original creation and signing of messages.
- `Oracle`: An entity that produces and distributes signed messages according to this specification.
- `Oracle feed`: A feed of signed messages from an oracle.
- `Oracle provider`: An entity that operates an oracle as a service.
- `Price message`: A strictly structured set of data that describes a price point.
- `Signed price message`: A price message with the corresponding signature from an oracle.
- `Signing key`: The private key that an oracle uses to sign price messages.
- `Smart contract`: A script that can lock value into a UTXO and is enforced by the blockchain.

## Requirements

For an oracle to be useful and trusted it needs to have a certain degree of consistency in terms of its content, availability and delivery.
The following rules exist to provide strong guarantees to contract makers regarding the oracle's behavior.

*Failure to follow any these requirements should be considered a violation and the oracle should not be used until the situation is properly addressed.*

### Isolated signing key

> A signing key must only be used for exactly one oracle feed, and must never be used for a different oracle feed or other purpose.

This ensures that a contract can trust that the information received will always match their expectations on what information is signed.
This also allows the oracle to properly isolate the signing key, reducing exposure and lowering the risks involved in key management.

### Minimum message production

> An oracle must always produce **at least one** price message for each **Attestation period**.

This ensures a minimum granularity of the oracle feed and allows contract makers to be certain there is never a gap in the provided data.

### Sequenced message production

> An oracle must always mark messages with a **message sequence number** in the order it produces all messages and must additionally always mark price messages with a **data sequence number** in the order it produces price messages.

The oracle sequence number ensures that contracts can rely on a chronological progression of time between price messages, and for contract service providers to detect problems such as when they are missing information (gaps in oracle sequence) or when there is suspected manipulation (multiple different messages with same oracle sequence number).

Sequence numbers start at **1** and are increased by **1** everytime a new **oracle message** is produced.
Data sequence numbers are start at **1** and are increased by **1** everytime a new **price message** is produced.

### Unique message production

> An oracle must never produce more than one message with the same sequence numbers.

The uniqueness of price messages is important to minimize uncertainty around contract executions.
The message and data sequence numbers must both be unique over the oracle's lifetime.

*Due to the uniqueness requirement, an oracle can never create more than 4,294,967,295 messages.*

### Timestamped message production

> An oracle must always mark price messages with a **timestamp** in the order it produces them.

This allows contracts to make decisions in relation to human timeframes and provides further accountability measures for the oracle provider.

## Recommendations

The following recommendations are not required but significantly improve the quality of an oracle and related infrastructure.

*Failure to follow these recommendations should be considered irresponsible, but not hostile.*

### Host isolation

> An oracle should produce and sign messages from a host isolated from the public delivery of messages.

This ensures that compromise of the known public host does not automatically compromise the signing key of the oracle.
This also adds a layer of protection against denial of service attacks and allows for scalability and redundancy of the message distribution infrastructure.

### Robust storage

Due to the uniqueness requirement for produced messages, it is important for storage to be robust.
For example, an oracle should store produced messages immediately and the storage should have strong persistence and retrieval guarantees.

## Message types

Oracles or other participants in an oracle network communicate two different types of messages:

Type | Description
--- | ---
Price message | Provides attestation for a price of an asset at a given time.
Oracle information | Adds or updates information about the signing oracle.


### Price messages

Price messages are the main type of data that Oracles produce.
They contain information about the price of an asset at a given point in time.

*All numbers are little-endian signed integers in order to be compatible with the **Bitcoin Cash** scripting engine.*

Part | Example | Size | Description
--- | --- | --- | ---
Message Timestamp | 1572882223 | 4 Bytes | Unix timestamp in UTC and seconds for the moment the oracle produced this message.
Message Sequence | 194421 |  4 Bytes | Sequence number for this price message relative to all of this oracle's messages.
Data Sequence | 194414 |  4 Bytes | Sequence number for this price message relative to all of this oracle's price messages.
Data Content | 412 | 4 bytes | Price of the asset.

### Oracle information messages

Oracles may publish information relating to the data they produce, the policies they follow, relevant events that might impact their users as well as owner or contact information.
These messages should be stored, broadcasted and relayed in the same manner as price messages.
They should use the same oracle sequence numbers and oracle timestamps as price messages in order to enforce the same expectations and consistency required of price messages.

It is expected that when a user or service requests oracle metadata, they will be presented with the most recently issued oracle message for each metadata type, unless explicitly requesting historical oracle metadata.
Oracles can publish another oracle message with the same metadata type in order to update that data.
If an Oracle updates a metadata type but uses an empty content, the data should be considered removed.
Some oracle metadata might not make sense to republish, such as the **Attestation factor** for the oracle, but this specification does not enforce specific behaviour in this regard.

*Interpretation of metadata and judgement of Oracle trustworthiness are left to Oracle users and services.*


Part | Example | Size | Description
--- | --- | --- | ---
Message Timestamp | 1572882223 | 4 Bytes | Unix timestamp in UTC and seconds for the moment the oracle produced this message.
Message Sequence | 194421 |  4 Bytes | Sequence number for this message relative to all of this oracle's messages.
Metadata Type | -1 |  4 Bytes | The type of the Metadata Content being described (see Metadata Types).
Metadata Content | "General Protocols" | Variable | The Metadata Content

### Oracle notification messages

In some cases it can be important for an oracle to make public information about some event that may impact oracle operation, security or other noteworthy condition. When such an event happens, the oracle may produce a notification message.

Part | Example | Size | Description
--- | --- | --- | ---
Oracle Timestamp | 1572882223 | 4 Bytes | Unix timestamp in UTC and seconds for the moment the oracle produced this message.
Message Sequence | 194421 |  4 Bytes | Sequence number for this message relative to all of this oracle's messages.
Notification Marker | 0 |  4 Bytes | Must be 0, which is both an invalid price sequence and an invalid metadata type.
Notification Message | "The signing key has been compromised" | Variable | The notification message explaining what has happened, as a UTF8 string.


## Delivery methods

We propose three different **delivery methods** to allow for different trade-offs between cost, availability, and reliability.
An oracle may use more than one delivery method.

### Request-Reply

The **Request-Reply delivery method** has a central service provider that delivers historical **oracle messages** on-demand when requested by authorized clients.
This allows a client to ask for messages related to a specific time, block or other information.
If more than one message matches the request parameters, the returned list is sorted by descending oracle sequence number.

Request can be implemented over any interactive protocol.
Regardless of which protocol is used, the structures will be the same or very similar.

Requesting information is useful when you want to get oracle metadata, or when a contract expires/matures at a known time.

![Client requests data from service](diagrams/client-server.png)

#### Requesting data

Requests should provide the required Oracle Public Key as well as optional [request parameters](#request-parameters).

##### ZeroMQ

An oracle that implements the **Request-Reply delivery method** over ZeroMQ should listen for requests on port `7083`.
The content of the request is a JSON-encoded object with the [request parameters](#request-parameters).

```js
// Request the first message this oracle has ever produced.
{
    publicKey: "0250863AD64A87AE8A2FE83C1AF1A8403CB53F53E486D8511DAD8A04887E5B2352",
    maxOracleSequence: 1,
    messageCount: 1,
}
```

##### WebSockets

TBD.

##### REST API

An oracle that implements the **Request-Reply delivery method** as a REST API should have an endpoint which accepts `GET` requests for oracle messages.
Request should include parameters for the required **public key** of the oracle as well as any optional [request parameters](#request-parameters).

For example, this URL requests the most current **oracle message** from the `02d09db08af1ff4e8453919cc866a4be427d7bfe18f2c05e5444c196fcf6fd2818` oracle,

> https://oracles.generalprotocols.com/api/v1/oracleMessages?publicKey=02d09db08af1ff4e8453919cc866a4be427d7bfe18f2c05e5444c196fcf6fd2818&count=1

This URL requests the first **oracle message** from the `02d09db08af1ff4e8453919cc866a4be427d7bfe18f2c05e5444c196fcf6fd2818` oracle on or preceding `2023-05-01 00:00:00` UTC.

> https://oracles.generalprotocols.com/api/v1/oracleMessages?publicKey=02d09db08af1ff4e8453919cc866a4be427d7bfe18f2c05e5444c196fcf6fd2818&maxMessageTimestamp=1682899200&count=1

###### Error reporting

Errors encountered during operation should be reported with a [HTTP status](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) and sent to the client as a JSON object containing the error status and a description of what went wrong.

```js
{
    'status': 'Service Unavilable',
    'description': 'The service is currently undergoing maintainance.'
}
```

The following problems should always use the indicated error codes:

Issue | Status Code | Description
--- | --- | ---
Missing parameter(s) | 400 | The request did not include the required parameters.
Missing oracle | 404 | The requested oracle could not be found.
Missing data | 404 | The requested data could not be found.
Payment required | 402 | The requested information is only available for paying customers.

Issue | Status Code | Description
--- | --- | ---
Service unavailable | 503 | The oracle is temporarily not operational, for example during maintenance or disaster recovery.

*When there is no suitable HTTP error code to reuse, a new name and number may be used that is within the appropriate group.*

#### Returning data

Responses should contain a JSON encoded array with 0 or more **signed message** objects.

```js
[
    {
        message: "7471000056220a0000000000000000000409654b3434dbf9cd1a77742e09c5a1def3716e2b2bdd770500fb050000cac2c75f"
        publicKey: "02bca289e12298914e45f1afdcb6e6fe822b9f9c7cd671fb195c476ac465725e",
        signature: "6f9670adaf2547079a36eb933bdd4c84b2e6910b1f0ea66c5dc1cc7435973e00b01b11b3cecfdf90ca277864bda8432629b3163088c291b6208511155859c5daa8",
    },
    {
        message: "1c71000056220a0000000000000000000409654b3434dbf9cd1a77742e09c5a1def3716e2b2bdd770600fc05000006c3c75f",
        publicKey: "02bca289e12298914e45f1afdcb6e6fe822b9f9c7cd671fb195c476ac465725e",
        signature: "6f7d69b9d0c2097ad4cccaf180e8842f2d4f8428de6af385a474cdb37c7a353f60cfd5754ba3e90f8cdffc01d660475576263d360c545427fdfd8ca1c2771e1072",
    },
    {
        message: "2471000056220a0000000000000000000409654b3434dbf9cd1a77742e09c5a1def3716e2b2bdd770700fd05000042c3c75f",
        publicKey: "02bca289e12298914e45f1afdcb6e6fe822b9f9c7cd671fb195c476ac465725e",
        signature: "6f20ca38e31baca4cf4c57f2b1e35450b1afaae059835c37613e87281545f45e40d71193803466436184f517a97aaf3917a31488e5e713f36a163379f2ce4c5b36",
    },
]
```

- *NOTE 1: The oracle service provider may limit the number of results per request.*
- *NOTE 2: The oracle service provider does not indicate if there are more messages available, so clients are expected to implement pagination locally by checking if they received fewer number of messages than they requested to determine when no more data is available.*


### Broadcast

The **broadcast delivery method** provides a central access point for peers to get a feed of continuously generated **oracle messages**.

![Service broadcasts data to clients](diagrams/server-push.png)

#### Implementations

##### ZeroMQ

An oracle that implements the broadcast delivery method over ZeroMQ should set up a **Publisher** socket on port `7084`.
Broadcasted messages should use the [message type](#message-types) as the topic.

##### WebSockets

TBD.

### Relay

The **relay delivery method** allows service providers to host and redistribute **Oracle Messages**.

By creating manually configured paths between oracles and public access services, a relay network is created. The relay network provides oracle producers with a way to achieve host isolation, create redundancy of both the data and access as well as improve the general reliability of the network.

It is technically possible to manually configure relays such that a loop is formed, and as such, relaying services should only relay each unique message once.

![data distributed through relays](diagrams/relay.png)

#### Implementations

##### ZeroMQ

An oracle that implements the relay delivery method over ZeroMQ should set up an incoming **Subscriber** socket to listen for messages, and an outgoing **Publisher** socket for each host to relay messages, all on port `7085`.

Relayed messages uses a one byte topic corresponding to the [message type](#message-types), and a JSON-encoded signed oracle message object as the content being delivered in each message.


## Failure recovery

No technology is perfect and sometimes implementations fail for one reason or another.
Such failure of the oracle could have catastrophic consequences and as such, implementations should respond consistently and predictably to failures.
The oracle must specifically pay attention to the contract makers needs.

In case of failure or other uncertainty, the oracle should confirm with available sources what messages the oracle has already produced.

For example:
- internal storage
- external indexers
- backups

### Operational outage

After an operational outage, it is important that the oracle does not naively resume operation but instead takes the necessary time and precautions to verify its internal state.

Once the internal state has been established, the oracle needs to roll-forward from the last published a message until the current time, producing and publishing intermediate price messages in chronological order.

Each Oracle can define their own strategy for this in their policy.
If no policy has been provided, the following strategy should be used:

1. If the oracle has access to historical pricing data, create the expected missing price messages using accurate historical data.
2. If the oracle is unable to retrieve historical pricing data, it should retrieve the current pricing data and create a set of price messages with a linear transition from the last known price to the current price.


# Appendix

## Metadata Types

### Oracle Information

Id | Name | Example | Format | Description
--- | --- | --- | --- | ---
-1 | OPERATOR_NAME | General Protocols | string | Human readable name of the operator of the oracle
-2 | OPERATOR_WEBSITE | https://generalprotocols.com/ | string | Link to more information on the operator of the oracle
-3 | RELAY_SERVER | https://oracles.generalprotocols.com/ | string | Link to relay server where this oracle's messages can be acquired
-4 | STARTING_TIMESTAMP | 1614691607 | Number | Unix timestamp at which users can expect the oracle to start reliably producing price messages
-5 | ENDING_TIMESTAMP | 1614691608 | Number | Unix timestamp at which users can expect the oracle to stop reliably producing price messages
-6 | ATTESTATION_SCALING | 100 | Number | Scaling value that the source price was multiplied by in order to retain precision when stored as an integer in oracle messages
-7 | ATTESTATION_PERIOD | 5000 | Number | How often the oracle creates a price message in milliseconds
-8 | OPERATOR_HASH | cd4f22417f6a7e8a050a401fe0513ee78abb58ca40692d2b80225e7bfe2f73d4 | string | Sha256 hash of the policy referenced by OPERATOR_WEBSITE


### Source Information

Id | Name | Example | Format | Description
--- | --- | --- | --- | ---
-51 | SOURCE_NAME | Binance | string | Human readable name of the data source the oracle observes
-52 | SOURCE_WEBSITE | https://www.binance.com/ | string | Link to more information on the data source the oracle
-53 | SOURCE_NUMERATOR_UNIT_NAME | US dollars | string | Human readable name of the original source data numerator.
-54 | SOURCE_NUMERATOR_UNIT_CODE | USD | String | Short code of the original source data numerator (Commonly ISO-4217 currency codes, but not required)
-55 | SOURCE_HASH | 6e1e0a96ada8e7ac5e91b660936ed406a696318c3a64324690c0d3d30950dcc1 | string | Sha256 hash of the policy referenced by SOURCE_WEBSITE
-56 | SOURCE_DENOMINATOR_UNIT_NAME | Bitcoin Cash | string | Human readable name of the original source data denominator.
-57 | SOURCE_DENOMINATOR_UNIT_CODE | BCH | String | Short code of the original source data denominator (Commonly ISO-4217 currency codes, but not required)


## Request parameters

### Message

parameter | required? | example | description
--- | --- | --- | ---
publicKey | yes | ... | public key of the oracle that signed the messages.
minMessageTimestamp | no | 1000000000 | earliest unix timestamp.
maxMessageTimestamp | no | 1234567890 | latest unix timestamp.
minMessageSequence | no | 1 | lowest message sequence.
maxMessageSequence | no | 9999 | highest message sequence.
minDataSequence | no | 1 | lowest data sequence.
maxDataSequence | no | 9999 | highest data sequence.
minMetadataType | no | 1 | lowest metadata type identifier.
maxMetadataType | no | 9999 | highest metadata type identifier.
count | no | 50 | maximum number of messages to return.
